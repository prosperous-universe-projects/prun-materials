package main

import (
	"flag"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
)

var (
	connectionString = flag.String("conn", getenvWithDefault("DATABASE_URL", ""), "PostgreSQL connection string")
	listenAddr       = flag.String("addr", getenvWithDefault("LISTENADDR", ":3000"), "HTTP address to listen on")
)

// App is a representation of the application
type App struct {
	db     *sqlx.DB
	Router *mux.Router
	Logger *log.Logger
}

const materialsTableCreationQuery = `create table if not exists materials
(
	id text,
	category text,
	name text,
	ticker text,
	volume real,
	weight real,
	isResource bool,
	resourceType text,
	inputRecipes json,
	outputRecipes json,
	buildingRecipes json,
	metadata json,
	constraint materials_pkey primary key (id)
)`

const materialCategoriesTableCreationQuery = `create table if not exists materialCategories
(
	id text,
	name text,
	constraint materialcategory_pkey primary key (id)
)`

func (a *App) connectDatabase(connectionString string) {

	var err error

	// Postrgres Connection

	if connectionString == "" {
		log.Fatalln("Please pass the connection string using the -conn option")
	}

	a.db, err = sqlx.Connect("pgx", connectionString)
	if err != nil {
		log.Fatalf("Unable to establish connection: %v\n", err)
	}
}

func getenvWithDefault(name, defaultValue string) string {
	val := os.Getenv(name)
	if val == "" {
		val = defaultValue
	}

	return val
}

func (a *App) ensureTablesExist() {
	if _, err := a.db.Exec(materialsTableCreationQuery); err != nil {
		log.Fatal(err)
	}
	if _, err := a.db.Exec(materialCategoriesTableCreationQuery); err != nil {
		log.Fatal(err)
	}
}

func main() {

	flag.Parse()

	a := App{}
	a.connectDatabase(*connectionString)
	a.ensureTablesExist()

	r := mux.NewRouter()

	r.HandleFunc("/api/v1/materialdata/ping", a.ping).Methods("GET")
	r.HandleFunc("/api/v1/materialdata/new", a.newMaterial).Methods("POST")
	r.HandleFunc("/api/v1/materialdata/all", a.getAllMaterials).Methods("GET")
	r.HandleFunc("/api/v1/materialdata/{id}", a.getMaterial).Methods("GET")
	r.HandleFunc("/api/v1/materialdata/{id}", a.updateMaterial).Methods("PUT")
	r.HandleFunc("/api/v1/materialdata/{id}", a.deleteMaterial).Methods("DELETE")

	r.HandleFunc("/api/v1/materialdata/categories/new", a.newMaterialCategory).Methods("POST")
	r.HandleFunc("/api/v1/materialdata/categories/all", a.getAllMaterialCategories).Methods("GET")
	r.HandleFunc("/api/v1/materialdata/categories/{id}", a.getMaterialCategory).Methods("GET")
	r.HandleFunc("/api/v1/materialdata/categories/{id}", a.deleteMaterialCategory).Methods("DELETE")

	log.Printf("listening on %s\n", *listenAddr)
	log.Fatal(http.ListenAndServe(*listenAddr, r))
}
