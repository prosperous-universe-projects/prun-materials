package main

import (
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/jmoiron/sqlx/types"

	"time"
)

// Material is a representation of the Material data from the database
type Material struct {
	ID              string         `json:"id" db:"id"`
	Category        string         `json:"category" db:"category"`
	Name            string         `json:"name" db:"name"`
	Ticker          string         `json:"ticker" db:"ticker"`
	Volume          float64        `json:"volume" db:"volume"`
	Weight          float64        `json:"weight" db:"weight"`
	IsResource      bool           `json:"isResource" db:"isresource"`
	ResourceType    string         `json:"resourceType" db:"resourcetype"`
	InputRecipes    types.JSONText `json:"inputRecipes" db:"inputrecipes"`
	OutputRecipes   types.JSONText `json:"outputRecipes" db:"outputrecipes"`
	BuildingRecipes types.JSONText `json:"buildingRecipes" db:"buildingrecipes"`
	MetaData        types.JSONText `json:"metadata" db:"metadata"`
}

// MaterialCategory is a representation of a Meterial group from the databse
type MaterialCategory struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

// EntryMetadata is related to the database entry itself, when was it last updated, who updated it, is it complete.
type EntryMetadata struct {
	Updated   time.Time `json:"updated"`
	Updater   string    `json:"updater"`
	Complete  bool      `json:"complete"`
	Validated bool      `json:"validated"`
	Validator string    `json:"validator"`
}

const (
	getMaterialsQuery   = "select * from materials"
	getMaterialQuery    = "select * from materials where id=$1 LIMIT 1"
	createMaterialQuery = `insert into materials(id, category, name, ticker, volume, weight, isResource, resourceType,
																inputRecipes, outputRecipes, buildingRecipes, metadata) values (:id, :category, :name, :ticker, :volume,
																:weight, :isresource, :resourcetype, :inputrecipes, :outputrecipes, :buildingrecipes, :metadata
																) returning id`
	deleteMaterialQuery = "delete from materials where id=$1"
	updateMaterialQuery = `update materials SET id=:id, category=:category, name=:name, ticker=:ticker, volume=:volume,
																weight=:weight, isResource=:isresource, resourceType=:resourcetype, inputRecipes=:inputrecipes,
																outputRecipes=:outputrecipes, buildingRecipes=:buildingrecipes, metadata=:metadata where id=$1`
	getMaterialCategoryQuery    = "select id, name from materialCategories"
	getMaterialCategoriesQuery  = "select id, name from materialCategories where id=$1"
	createMaterialCategoryQuery = "insert into materialCategories(id, name) values ($1, $2) returning id"
	deleteMaterialCategoryQuery = "delete from materialCategories where id=$1"
)

// Material Category Models

func (m *MaterialCategory) deleteMaterialCategory(db *sqlx.DB) error {
	_, err := db.Exec(deleteMaterialCategoryQuery, m.ID)
	return err
}

func (m *MaterialCategory) getMaterialCategory(db *sqlx.DB) error {
	err := db.QueryRow(getMaterialCategoryQuery, m.ID).Scan(&m.ID, &m.Name)
	if err != nil {
		return err
	}
	return nil
}

func (m *MaterialCategory) newMaterialCategory(db *sqlx.DB) error {
	err := db.QueryRow(createMaterialCategoryQuery, m.ID, m.Name).Scan(&m.ID)
	if err != nil {
		return err
	}
	return nil
}

func getMaterialCategories(db *sqlx.DB) ([]MaterialCategory, error) {
	rows, err := db.Query(getMaterialCategoriesQuery)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	allMaterialCategories := []MaterialCategory{}

	for rows.Next() {
		var m MaterialCategory
		if err := rows.Scan(&m.ID, &m.Name); err != nil {
			return nil, err
		}
		allMaterialCategories = append(allMaterialCategories, m)
	}
	return allMaterialCategories, err
}
