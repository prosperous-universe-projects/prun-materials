package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"
	"github.com/gorilla/mux"
)

// send a payload of JSON content
func (a *App) respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

// send a JSON error message
func (a *App) respondWithError(w http.ResponseWriter, code int, message string) {
	a.respondWithJSON(w, code, map[string]string{"error": message})

	log.Printf("App error: code %d, message %s", code, message)
}

func (a *App) ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Pong")
}

// Material Handlers

func (a *App) newMaterial(w http.ResponseWriter, r *http.Request) {
	var p Material
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&p); err != nil {
		msg := fmt.Sprintf("Invalid request payload. Error: %s", err.Error())
		a.respondWithError(w, http.StatusBadRequest, msg)
		return
	}

	defer r.Body.Close()

	_, err := a.db.NamedExec(createMaterialQuery, &p)
	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	a.respondWithJSON(w, http.StatusCreated, p)
}

func (a *App) getAllMaterials(w http.ResponseWriter, r *http.Request) {

	var p []Material

	err := a.db.Select(&p, getMaterialsQuery)

	if err != nil {
		log.Println("Getting material from the database failed")
		switch err {
		case sql.ErrNoRows:
			msg := fmt.Sprintf("Material not found. Error: %s", err.Error())
			a.respondWithError(w, http.StatusNotFound, msg)
		default:
			a.respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	a.respondWithJSON(w, http.StatusOK, p)
}

func (a *App) getMaterial(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	log.Println(id)

	var p Material
	err := a.db.Get(&p, getMaterialQuery, id)
	if err != nil {
		log.Println("Getting material from the database failed")
		switch err {
		case sql.ErrNoRows:
			msg := fmt.Sprintf("Material not found. Error: %s", err.Error())
			a.respondWithError(w, http.StatusNotFound, msg)
		default:
			a.respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	a.respondWithJSON(w, http.StatusOK, p)
}

func (a *App) updateMaterial(w http.ResponseWriter, r *http.Request) {
	var p Material
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&p); err != nil {
		msg := fmt.Sprintf("Invalid request payload. Error: %s", err.Error())
		a.respondWithError(w, http.StatusBadRequest, msg)
		return
	}

	defer r.Body.Close()

	_, err := a.db.NamedExec(updateMaterialQuery, &p)
	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	a.respondWithJSON(w, http.StatusCreated, p)
}

func (a *App) deleteMaterial(w http.ResponseWriter, r *http.Request) {
	var p Material
	vars := mux.Vars(r)
	id := vars["id"]

	p.ID = id

	_, err := a.db.Exec(deleteMaterialQuery, id)
	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
	}

	a.respondWithJSON(w, http.StatusOK, p)
}

// Material Category Handlers

func (a *App) newMaterialCategory(w http.ResponseWriter, r *http.Request) {
	var m MaterialCategory
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&m); err != nil {
		msg := fmt.Sprintf("Invalid request payload. Error: %s", err.Error())
		a.respondWithError(w, http.StatusBadRequest, msg)
		return
	}

	defer r.Body.Close()

	if err := m.newMaterialCategory(a.db); err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	a.respondWithJSON(w, http.StatusCreated, m)
}

func (a *App) getAllMaterialCategories(w http.ResponseWriter, r *http.Request) {

	materialCategories, err := getMaterialCategories(a.db)

	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	a.respondWithJSON(w, http.StatusOK, materialCategories)
}

func (a *App) getMaterialCategory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	log.Println(id)

	// if err := validation.Validate(id, is.UUID); err != nil {
	// 	log.Println("ID failed validation")
	// 	msg := fmt.Sprintf("ID is not valid, must be valid UUID. Error: %s", err.Error())
	// 	a.respondWithError(w, http.StatusBadRequest, msg)
	// 	return
	// }

	m := MaterialCategory{ID: id}
	if err := m.getMaterialCategory(a.db); err != nil {
		log.Println("Getting material Category from the database failed")
		switch err {
		case sql.ErrNoRows:
			msg := fmt.Sprintf("Material Category not found. Error: %s", err.Error())
			a.respondWithError(w, http.StatusNotFound, msg)
		default:
			a.respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	a.respondWithJSON(w, http.StatusOK, m)
}

func (a *App) deleteMaterialCategory(w http.ResponseWriter, r *http.Request) {
	var m MaterialCategory
	vars := mux.Vars(r)
	id := vars["id"]

	// if err := validation.Validate(id, is.UUID); err != nil {
	// 	log.Println("ID failed validation")
	// 	msg := fmt.Sprintf("ID is not valid, must be valid UUID. Error: %s", err.Error())
	// 	a.respondWithError(w, http.StatusBadRequest, msg)
	// 	return
	// }

	m.ID = id

	if err := m.deleteMaterialCategory(a.db); err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
	}

	a.respondWithJSON(w, http.StatusOK, m)
}
